export enum KeyEventType {
  Down,
  Up,
}

export interface KeyEvent {
  timeMs: number;
  type: KeyEventType;
  isHandled: boolean;
}

export class Keyboard {
  private keyEvents: Map<string, KeyEvent> = new Map();
  private repeatThresholdMs: number;
  public watchedKeys: string[];

  public constructor(watchedKeys: string[], repeatThresholdMs: number = 10) {
    this.repeatThresholdMs = repeatThresholdMs;
    this.watchedKeys = watchedKeys;

    const { keyEvents } = this;
    window.addEventListener("keydown", (ev: KeyboardEvent) => {
      const { code } = ev;
      if (watchedKeys.includes(code)) ev.preventDefault();
      const existingEvent = keyEvents.get(code);
      if (!existingEvent || existingEvent.type === KeyEventType.Up) {
        keyEvents.set(code, {
          timeMs: new Date().getTime(),
          type: KeyEventType.Down,
          isHandled: false,
        });
      }
    });
    window.addEventListener("keyup", (ev: KeyboardEvent) => {
      const { code } = ev;
      if (watchedKeys.includes(code)) ev.preventDefault();
      keyEvents.set(code, {
        timeMs: new Date().getTime(),
        type: KeyEventType.Up,
        isHandled: false,
      });
    });
  }

  public forEachActiveKey(
    callback: (key: string, keyEvent: KeyEvent, nowMs: number) => void
  ): void {
    const { repeatThresholdMs } = this;
    const now = new Date().getTime();

    this.keyEvents.forEach((event, key) => {
      const { isHandled, timeMs, type } = event;
      switch (type) {
        case KeyEventType.Down:
          const pastThreshold = now > timeMs + repeatThresholdMs;
          if (pastThreshold) {
            callback(key, event, now);
            event.timeMs = now;
          }
          break;
        case KeyEventType.Up:
          if (!isHandled) {
            callback(key, event, now);
            event.isHandled = true;
          }
          break;
      }
    });
  }

  public getActiveKeys(): string[] {
    const { repeatThresholdMs } = this;
    const now = new Date().getTime();
    const active: string[] = [];

    this.keyEvents.forEach((event, key) => {
      const { isHandled, timeMs, type } = event;
      switch (type) {
        case KeyEventType.Down:
          const pastThreshold = now > timeMs + repeatThresholdMs;
          if (pastThreshold) {
            active.push(key);
            event.timeMs = now;
          }
          break;
        case KeyEventType.Up:
          if (!isHandled) {
            active.push(key);
            event.isHandled = true;
          }
          break;
      }
    });
    return active;
  }
}

// the initial seed
let seed = 42;

// in order to work 'Math.seed' must NOT be undefined,
// so in any case, you HAVE to provide a Math.seed
export function seededRandom(min: number = 0, max: number = 1): number {
  seed = (seed * 9301 + 49297) % 233280;
  var rnd = seed / 233280;

  return min + rnd * (max - min);
}

export function setRandomSeed(newSeed: number): void {
  seed = newSeed;
}

export const ProjectName = getEnvVarValue("CI_PROJECT_NAME", "flight3d");
export const CommitSha = getEnvVarValue("CI_COMMIT_SHA");
export const CommitBranch = getEnvVarValue("CI_COMMIT_BRANCH");

function getEnvVarValue(value: string, defaultValue: string = "local"): string {
  const isReplaced = !value.startsWith("CI_");
  const actual = isReplaced ? value : defaultValue;
  return actual;
}

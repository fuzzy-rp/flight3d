import { Vector3 } from "three";

interface BaseAgent {
  pos: Vector3;
  velocity: Vector3;
  maxSpeed: number;
}
export interface Agent extends BaseAgent {
  heading: Vector3;
  radius: number;
  side: Vector3;
  speed: number;
}

export function calcForce(desiredVel: Vector3, velocity: Vector3) {
  return new Vector3().subVectors(desiredVel, velocity);
}
// export function pointToWorldSpace(
//   local: vec2,
//   heading: vec2,
//   side: vec2,
//   pos: vec2,
//   camera: Camera
// ): vec2 {
//   const matrix = rotateMatrix(heading, side)
//     .multiply(translateMatrix(pos))
//     .multiply(translateMatrix(camera.pos));

//   const world = new vec2();
//   matrix.multiplyVec2(local, world); // .transform(local.xy, world.xy, 1); //.applyToPoint(total, local);
//   return world;
// }
// function vectorToWorldSpace(vec: vec2, heading: vec2, side: vec2): vec2 {
//   const matrix = rotateMatrix(heading, side);
//   const world = new vec2();
//   matrix.multiplyVec2(vec, world); // .transform(vec.xy, world.xy, 1);
//   return world;
// }
// export function pointToLocalSpace(
//   world: vec2,
//   heading: vec2,
//   side: vec2,
//   pos: vec2
// ): vec2 {
//   const negPos = pos.copy().scale(-1);
//   const tx = vec2.dot(negPos, heading);
//   const ty = vec2.dot(negPos, side);

//   // prettier-ignore
//   const matrix = new mat3([
//     heading.x, side.x, 0,
//     heading.y, side.y, 0,
//     tx, ty, 1
//   ]);

//   const local = new vec2();
//   matrix.multiplyVec2(world, local);
//   return local;
// }
// function rotateMatrix(fwd: vec2, side: vec2): mat3 {
//   // prettier-ignore
//   return new mat3([
//     fwd.x, fwd.y, 0,
//     side.x, side.y, 0,
//     0, 0, 1]);
// }
// function translateMatrix(pos: vec2): mat3 {
//   // prettier-ignore
//   return new mat3([
//     1, 0, 0,
//     0, 1, 0,
//     pos.x, pos.y, 1]);
// }

// export function seek({ pos, velocity, maxSpeed }: BaseAgent, dest: vec2): vec2 {
//   const desiredVel: vec2 = dest
//     .copy()
//     .subtract(pos)
//     .normalize()
//     .scale(maxSpeed);
//   return calcForce(desiredVel, velocity);
// }

// export function flee({ pos, velocity, maxSpeed }: BaseAgent, dest: vec2): vec2 {
//   const desiredVel: vec2 = pos
//     .copy()
//     .subtract(dest)
//     .normalize()
//     .scale(maxSpeed);
//   return calcForce(desiredVel, velocity);
// }

export function arrive(
  { pos, velocity, maxSpeed }: BaseAgent,
  dest: Vector3,
  deceleration: 1 | 2 | 3
): Vector3 {
  if (dest === undefined) alert("dest undefined");
  if (pos === undefined) alert("pos undefined");
  const toTarget = new Vector3().subVectors(dest, pos);
  const dist = toTarget.length();

  if (dist > 0) {
    const decTweaker = 0.3;
    let speed = dist / (deceleration * decTweaker);
    speed = Math.min(speed, maxSpeed);
    const desiredVel = toTarget.clone().multiplyScalar(speed / dist);
    const force = calcForce(desiredVel, velocity);
    return force;
  }

  return new Vector3();
}

// export function pursue(): vec2 {
//   // page 117 of Programming AI by Example PDF
//   throw new Error(`Not implemented`);
// }

// export function evade(): vec2 {
//   // page 119 of Programming AI by Example PDF
//   throw new Error(`Not implemented`);
// }

// interface WanderProps {
//   wanderRadius: number;
//   wanderDistance: number;
//   wanderJitter: number;
// }
// export class WanderBehavior {
//   public props: WanderProps;
//   private wanderTarget: vec2;
//   private theta: number;

//   public constructor(props: WanderProps) {
//     this.props = props;
//     const { wanderRadius } = props;
//     this.theta = Math.random() * Math.PI * 2;
//     this.wanderTarget = new vec2([
//       wanderRadius * Math.cos(this.theta),
//       wanderRadius * Math.sin(this.theta)
//     ]);
//   }
//   public wander(agent: Agent, dt: number): vec2 {
//     const {
//       props: { wanderDistance, wanderRadius, wanderJitter },
//       wanderTarget
//     } = this;
//     const { heading, pos, side } = agent;
//     // page 119 of Programming AI by Example PDF
//     wanderTarget
//       .add(randomClamped().scale(wanderJitter * dt, new vec2()))
//       .normalize()
//       .scale(wanderRadius);
//     const targetLocal = new vec2([
//       wanderTarget.x + wanderDistance,
//       wanderTarget.y
//     ]);
//     const targetWorld = pointToWorldSpace(targetLocal, heading, side, pos);
//     const steerTo = targetWorld.subtract(pos, new vec2());
//     return steerTo;
//   }
// }

// interface Obstacle {
//   pos: vec2;
//   radius: number;
// }
// export function avoidObstacles(
//   agent: Agent,
//   obstacles: () => Obstacle[]
// ): vec2 {
//   // page 122 of Programming AI by Example PDF
//   const minDBoxLength = 50;
//   const dBoxLength =
//     minDBoxLength + (agent.speed / agent.maxSpeed) * minDBoxLength;

//   interface closestAgent {
//     agent?: Obstacle;
//     dist: number;
//     localPos: vec2;
//   }
//   const closestAgent: closestAgent = {
//     dist: Number.MAX_VALUE,
//     localPos: vec2.zero
//   };

//   const allObstacles = obstacles();
//   for (const ob of allObstacles) {
//     const localPos = pointToLocalSpace(
//       ob.pos,
//       agent.heading,
//       agent.side,
//       agent.pos
//     );
//     // log({
//     //   localPos: localPos.xy,
//     //   obPos: ob.pos.xy,
//     //   heading: agent.heading.xy,
//     //   side: agent.side.xy,
//     //   pos: agent.pos.xy
//     // });
//     if (localPos.x >= 0) {
//       const expRadius = ob.radius + agent.radius;
//       if (Math.abs(localPos.y) < expRadius) {
//         const { x: cx, y: cy } = localPos;
//         const sqrtPart = Math.sqrt(expRadius * expRadius - cy * cy);
//         let ip = cx - sqrtPart;
//         if (ip <= 0) {
//           ip = cx + sqrtPart;
//         }

//         if (ip < closestAgent.dist) {
//           closestAgent.agent = ob;
//           closestAgent.dist = ip;
//           closestAgent.localPos = localPos;
//         }
//       }
//     }
//   }

//   // log({ closestAgent });

//   if (closestAgent.agent) {
//     const steeringForce = new vec2();
//     const multiplier = 1 + (dBoxLength - closestAgent.localPos.x) / dBoxLength;
//     steeringForce.y =
//       (closestAgent.agent.radius - closestAgent.localPos.y) * multiplier;
//     const brakingWeight = 0.2;
//     steeringForce.x =
//       (closestAgent.agent.radius - closestAgent.localPos.x) * brakingWeight;
//     const world = vectorToWorldSpace(steeringForce, agent.heading, agent.side);
//     log({ world });
//   }

//   return vec2.zero;
// }

// export function steering(): vec2 {
//   // page 126 of Programming AI by Example PDF
//   throw new Error(`Not implemented`);
// }

// export function avoidWalls(): vec2 {
//   // page 127 of Programming AI by Example PDF
//   throw new Error(`Not implemented`);
// }

// export function interpose(): vec2 {
//   // page 129 of Programming AI by Example PDF
//   throw new Error(`Not implemented`);
// }

// export function hide(): vec2 {
//   // page 130 of Programming AI by Example PDF
//   throw new Error(`Not implemented`);
// }

// export function pathFollow(): vec2 {
//   // page 133 of Programming AI by Example PDF
//   throw new Error(`Not implemented`);
// }

// export function offsetPursue(): vec2 {
//   // page 134 of Programming AI by Example PDF
//   throw new Error(`Not implemented`);
// }

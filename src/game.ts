import * as THREE from "three";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import { Flyer } from "./flyer";
import { CommitSha, ProjectName, CommitBranch } from "./utils/envVars";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls.js";
import Stats from "three/examples/jsm/libs/stats.module.js";
import { GUI } from "dat.gui";
import { ForceSource, Source } from "./physics/forceSource";
import { Vector3, Vector2 } from "three";
import { seededRandom, setRandomSeed } from "./utils/seededRandom";
import { debugBuffer, log } from "./utils/log";
import { Keyboard } from "./utils/keyboard";
import { Props, State, UIInputState, GameContext } from "./gameInterfaces";
import { arrive } from "./physics/forces";

export class Game {
  private props: Props;
  private state: State;

  public constructor(
    props: Props = {
      backgroundColor: 0xadd8e6,
      groundSize: 20,
      maxHeight: 100,
      camera: {
        pos: new THREE.Vector3(-40, 40, 0),
        lookAt: new THREE.Vector3(2.5, 0.5, 0),
      },
      flyer: new Flyer({
        pos: new THREE.Vector3(0, 2, 4),
        radius: 1000,
        heading: new THREE.Vector3(0, 0, -1),
        rotation: new THREE.Vector3(),
        up: new THREE.Vector3(0, 1, 0),
        maxSelfForce: 10,
        maxSpeed: 3,
        velocity: new THREE.Vector3(0, 0, -3),
      }),
    }
  ) {
    console.error(
      `Hi from project ${ProjectName}, branch ${CommitBranch}, commit ${CommitSha}`
    );

    setRandomSeed(1984);

    this.props = props;
    if (!props.flyer.props.forceSource) {
      props.flyer.props.forceSource = this.createForceSource();
    }
    const { groundSize } = props;

    // SCENE
    const scene = new THREE.Scene();
    scene.background = new THREE.Color(props.backgroundColor);

    // CAMERA
    const camera = new THREE.PerspectiveCamera();
    camera.position.copy(props.camera.pos);
    camera.lookAt(props.camera.lookAt);

    // RADAR CAMERA
    const radarCamera = new THREE.OrthographicCamera(
      -groundSize - 2,
      groundSize + 2,
      groundSize + 2,
      -groundSize - 2,
      1,
      groundSize * 2
    );
    radarCamera.position.copy(new THREE.Vector3(0, 20, 0));
    radarCamera.lookAt(new THREE.Vector3());

    // RENDERER
    const renderer = new THREE.WebGLRenderer({
      antialias: true,
      preserveDrawingBuffer: true,
    });
    renderer.shadowMap.enabled = true;
    renderer.shadowMap.type = THREE.PCFSoftShadowMap; // default THREE.PCFShadowMap
    renderer.autoClear = false;
    renderer.autoClearColor = false;
    renderer.autoClearDepth = false;
    renderer.autoClearStencil = false;
    renderer.setSize(window.innerWidth, window.innerHeight);
    document.body.appendChild(renderer.domElement);

    // CAMERA CONTROLS
    const cameraControls = new OrbitControls(camera, renderer.domElement);

    // RAY CASTER
    var raycaster = new THREE.Raycaster();

    // LIGHTS
    const ambientLight = new THREE.AmbientLight("white", 0.8);
    ambientLight.castShadow = false;
    scene.add(ambientLight);
    const light = new THREE.DirectionalLight("white", 1);
    light.position.set(0, 30, 0); //default; light shining from top
    light.castShadow = true;
    light.shadow.camera.top = groundSize;
    light.shadow.camera.bottom = -groundSize;
    light.shadow.camera.left = -groundSize;
    light.shadow.camera.right = groundSize;
    light.shadow.camera.near = 20;
    light.shadow.camera.far = 31;
    light.shadow.radius = 2;
    light.shadow.camera.updateProjectionMatrix();
    scene.add(light);

    // CAMERA HELPERS
    // scene.add(new THREE.CameraHelper(light.shadow.camera));
    // scene.add(new THREE.CameraHelper(radarCamera));

    // GROUND
    const ground = new THREE.Mesh(
      new THREE.PlaneBufferGeometry(groundSize * 2, groundSize * 2),
      new THREE.MeshPhongMaterial({ color: "white" })
    );
    ground.rotation.x = -Math.PI / 2;
    ground.receiveShadow = true;
    scene.add(ground);

    // CUBE
    const geometry = new THREE.BoxGeometry();
    const material = this.createUVMaterial();
    const cube = new THREE.Mesh(geometry, material);
    cube.castShadow = true;
    cube.position.add(new THREE.Vector3(2.5, 0.5, 0));
    scene.add(cube);

    // GUI
    const uiInput: UIInputState = {
      isPaused: false,
      isFoggy: false,
      fogDistance: { near: 1, far: groundSize },
      camera: {
        followDistance: 1,
        followGlider: false,
      },
      isAutopilotOn: true,
    };
    this.buildGui(uiInput, camera, props);

    // STATS
    const stats = Stats();
    document.body.appendChild(stats.dom);

    // DOCUMENT/WINDOW UPDATES
    let mouseDownPos: Vector2 = null;
    window.addEventListener("resize", () => this.resize());
    window.addEventListener("mousedown", (me) => {
      mouseDownPos = new THREE.Vector2(me.clientX, me.clientY);
    });
    window.addEventListener("mouseup", (me: MouseEvent) => {
      const { followGlider } = uiInput.camera;
      const currentPos = new THREE.Vector2(me.clientX, me.clientY);
      const distMoved = mouseDownPos
        ? mouseDownPos.distanceTo(currentPos)
        : Number.MAX_VALUE;
      mouseDownPos = null;
      if (!followGlider && distMoved < 5) {
        const x = (me.clientX / window.innerWidth) * 2 - 1;
        const y = ((me.clientY / window.innerHeight) * 2 - 1) * -1;
        const vec2 = new THREE.Vector2(x, y);
        raycaster.setFromCamera(vec2, camera);
        const intersects = raycaster.intersectObject(ground);
        const first = intersects[0];
        if (first) {
          const flyerPos = this.props.flyer.props.pos;
          this.state.game.target = first.point.clone().setY(flyerPos.y);
        }
      }
    });
    window.addEventListener("keyup", (ke: KeyboardEvent) => {
      switch (ke.code) {
        case "KeyP":
          uiInput.isPaused = !uiInput.isPaused;
          break;
      }
    });

    // CREATE STATE
    this.state = {
      uiInput,
      internal: {
        renderer,
        camera,
        cameraControls,
        // radarCamera,
        lastAnimateMs: new Date().getTime(),
        stats,
        cube,
        scene,
        keyboard: new Keyboard(["KeyA", "KeyD"]),
        // gridGeometry:
        //   ground.geometry instanceof THREE.BufferGeometry
        //     ? new THREE.Geometry().fromBufferGeometry(ground.geometry)
        //     : ground.geometry,
      },
      game: {
        isGameOver: false,
      },
    };

    // LAZY-LOADED GLIDER
    const { state } = this;
    var loader = new GLTFLoader();
    loader.load(
      "models/glider/scene.gltf",
      (gltf) => {
        try {
          gltf.scene.scale.multiplyScalar(0.01);
          gltf.scene.position.add(new THREE.Vector3(100, 100, 0));
          gltf.scene.traverse((node) => {
            if ((node as any).isMesh) {
              node.castShadow = true;
            }
          });

          state.internal.glider = gltf.scene;
          scene.add(gltf.scene);
        } catch (error) {
          console.error({ message: "issue loading glider", error });
        }
      },
      undefined,
      (error) => {
        console.error({ error });
      }
    );

    // START ANIMATING
    this.resize();
    this.animate();
  }

  private buildGui(
    uiInput: UIInputState,
    camera: THREE.PerspectiveCamera,
    props: Props,
    cameraControls?: OrbitControls
  ) {
    const { groundSize } = props;
    const gui = new GUI();

    gui.add(uiInput, "isPaused", false).name("Paused").listen();
    gui.add(uiInput, "isAutopilotOn").name("Auto-pilot").listen();

    const fogGui = gui.addFolder("Fog");
    fogGui.open();
    fogGui.add(uiInput, "isFoggy", false).name("Fog").listen();
    fogGui
      .add(uiInput.fogDistance, "near", 1, groundSize, 0.1)
      .name("Near Distance")
      .listen()
      .onChange(() => {
        this.setFog();
      });
    fogGui
      .add(uiInput.fogDistance, "far", 1, groundSize * 3, 0.1)
      .name("Far Distance")
      .listen()
      .onChange(() => {
        this.setFog();
      });

    const cameraGui = gui.addFolder("Camera");
    cameraGui.open();
    cameraGui
      .add(uiInput.camera, "followGlider")
      .name("Follow Glider")
      .onChange(() => {
        if (cameraControls) {
          cameraControls.update();
        }
        if (uiInput.camera.followGlider) {
          // uiInput.isFoggy = true;
        } else {
          camera.position.copy(props.camera.pos);
          camera.lookAt(props.camera.lookAt);
          // uiInput.isFoggy = false;
        }
      });
    cameraGui
      .add(uiInput.camera, "followDistance", 1, groundSize * 2, 0.1)
      .name("Follow Distance");
  }

  private createUVMaterial() {
    var textureMap = new THREE.TextureLoader().load(
      "textures/uv_grid_opengl.jpg"
    );
    textureMap.wrapS = textureMap.wrapT = THREE.RepeatWrapping;
    textureMap.anisotropy = 16;
    textureMap.encoding = THREE.sRGBEncoding;
    const materialColor = new THREE.Color("white");
    const material = new THREE.MeshPhongMaterial({
      color: materialColor,
      map: textureMap,
      side: THREE.DoubleSide,
    });
    return material;
  }

  private createForceSource(): ForceSource {
    const thisRef = this;

    const fs = new ForceSource({
      currentState: "user-input",
      stateToSourceMap: new Map<string, Source>([
        [
          "user-input",
          (): Vector3[] => {
            const forces = [];
            if (thisRef.state.game.userForce) {
              forces.push(thisRef.state.game.userForce);
              thisRef.state.game.userForce = null;
            }
            return forces;
          },
        ],
        [
          "hunt",
          (ship): Vector3[] => {
            const forces = [];
            const dest = thisRef.state.game.target;
            if (dest) {
              const dist = dest.distanceTo(ship.pos);
              if (dist > 2) {
                const arriveForce = arrive(ship, dest, 3);
                forces.push(arriveForce);
              }
            }
            return forces;
          },
        ],
      ]),
    });
    return fs;
  }

  private getRandomVec3(box: THREE.Box3): THREE.Vector3 {
    console.error({ box });
    return new THREE.Vector3(
      seededRandom(box.min.x, box.max.x),
      seededRandom(box.min.y, box.max.y),
      seededRandom(box.min.z, box.max.z)
    );
  }

  private resize() {
    const {
      state: {
        internal: { renderer, camera },
      },
    } = this;
    const { innerWidth, innerHeight } = window;

    const aspect = innerWidth / innerHeight;

    camera.aspect = aspect;
    camera.updateProjectionMatrix();

    renderer.setSize(innerWidth, innerHeight);
  }

  private animate() {
    requestAnimationFrame(() => this.animate());

    const {
      state: {
        uiInput: { isFoggy },
        internal: { renderer, stats, scene, camera, radarCamera },
      },
    } = this;

    this.step();

    this.updateGlider();

    if (isFoggy && scene.fog === null) {
      this.setFog();
    } else if (!isFoggy && scene.fog !== null) {
      scene.fog = null;
    }

    const { innerWidth, innerHeight } = window;

    renderer.clear();

    renderer.setViewport(0, 0, innerWidth, innerHeight);
    renderer.render(scene, camera);

    if (radarCamera) {
      const width = 200,
        height = 200;
      renderer.setViewport(innerWidth - width, 0, width, height);
      renderer.render(scene, radarCamera);
    }

    stats.update();

    const debugDiv = document.getElementById("debug-div");
    if (debugDiv) {
      debugDiv.innerHTML = debugBuffer.join("<br />");
    }
    debugBuffer.length = 0;
  }

  private setFog() {
    const { scene } = this.state.internal;
    const { near, far } = this.state.uiInput.fogDistance;
    const { backgroundColor } = this.props;
    scene.fog = new THREE.Fog(backgroundColor, near, far);
  }

  private updateGlider() {
    const {
      props: { flyer },
      state: {
        internal: { camera, glider },
        uiInput,
      },
    } = this;

    if (glider) {
      const {
        props: {
          heading,
          pos,
          pos: { x, y, z },
        },
      } = flyer;
      glider.position.set(x, y - 0.5, z);
      const headingHuge = heading.clone().multiplyScalar(1000);
      const lookAt = pos.clone().add(headingHuge);
      glider.lookAt(lookAt);
      const {
        camera: { followGlider, followDistance },
      } = uiInput;
      if (followGlider) {
        camera.position.set(x, y, z);
        camera.position.add(heading.clone().multiplyScalar(-followDistance));
        camera.lookAt(pos);
      }
    }
  }

  private step() {
    const {
      props: {
        groundSize,
        flyer,
        flyer: {
          props: { forceSource, pos },
        },
      },
      state: {
        uiInput,
        uiInput: { isPaused, isAutopilotOn },
        game,
        game: { isGameOver },
        internal: { lastAnimateMs, cube, keyboard },
      },
    } = this;

    const nowMs = new Date().getTime();
    const diffMs = nowMs - lastAnimateMs;
    this.state.internal.lastAnimateMs = nowMs;

    if (isPaused || isGameOver) return;

    const dt = diffMs / 1000;
    if (dt < 0 || dt > 1) {
      console.error(`invalid dt: ${dt}`);
      uiInput.isPaused = true;
    } else {
      const context: GameContext = { dt };

      if (forceSource) {
        forceSource.setCurrentState(isAutopilotOn ? "hunt" : "user-input");
      }

      if (game.target) {
        cube.position.copy(game.target).setY(2.5);

        const dist = pos.distanceTo(game.target.clone().setY(pos.y));
        if (dist < 1) {
          game.target = null;
        }
      } else {
        // if (gridGeometry.boundingBox === null) {
        //   gridGeometry.computeBoundingBox();
        // }
        // const bounds = gridGeometry.boundingBox;
        const bounds: THREE.Box3 = new THREE.Box3(
          new THREE.Vector3(-groundSize, 0, -groundSize),
          new THREE.Vector3(groundSize, 0, groundSize)
        );
        const point = this.getRandomVec3(bounds);
        point.y = pos.y;
        game.target = point;
      }

      const activeKeys = keyboard.getActiveKeys();
      const aDown = activeKeys.includes("KeyA");
      const dDown = activeKeys.includes("KeyD");
      if (aDown || dDown) {
        uiInput.isAutopilotOn = false;
        const {
          props: { heading, up, maxSelfForce },
        } = flyer;
        const right = new Vector3().crossVectors(heading, up);
        const direction = dDown ? 1 : -1;

        game.userForce = right.multiplyScalar((maxSelfForce / 3) * direction);
      }

      flyer.step(context);

      this.testGameEnd(flyer);
    }
  }

  private testGameEnd(flyer: Flyer) {
    const { pos } = flyer.props;
    if (pos.y < 0) {
      this.state.game.isGameOver = true;
      alert(`Game Over!`);
    }
  }
}
